# Italian translation for dice-roller
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the dice-roller package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: dice-roller\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-11 05:59+0000\n"
"PO-Revision-Date: 2016-05-09 23:34+0000\n"
"Last-Translator: Emanuele Antonio Faraone <emanueleant03@gmail.com>\n"
"Language-Team: Italian <it@li.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-06-18 06:07+0000\n"
"X-Generator: Launchpad (build 18097)\n"

#: ../qml/MainPage.qml:24 ../qml/AboutPage.qml:33 dice-roller.desktop.in.h:1
msgid "Dice Roller"
msgstr "Dice Roller"

#: ../qml/MainPage.qml:29 ../qml/AboutPage.qml:8
msgid "About"
msgstr ""

#: ../qml/MainPage.qml:201
msgid ""
"There are no dice to save\n"
"Try adding dice before saving to a collection"
msgstr ""

#: ../qml/MainPage.qml:210
msgid "Your custom die has been removed"
msgstr ""

#: ../qml/MainPage.qml:219
msgid "Your collection has been removed"
msgstr ""

#: ../qml/MainPage.qml:228
msgid ""
"Your color choice is invalid.\n"
"Try using colors from\n"
"\"http://doc.qt.io/qt-5/qml-color.html\""
msgstr ""

#: ../qml/MainPage.qml:275
#, fuzzy
#| msgid "Dice Roller"
msgid "Welcome to Dice Roller!"
msgstr "Dice Roller"

#: ../qml/MainPage.qml:284
msgid "Tap the plus and minus to add or remove dice."
msgstr ""

#: ../qml/MainPage.qml:292
msgid "Tap the dice button to select what kind of die to add."
msgstr ""

#: ../qml/MainPage.qml:300
msgid "Tap the refresh button to reroll."
msgstr ""

#: ../qml/MainPage.qml:308
msgid "Swipe up from the bottom to access settings and create custom die."
msgstr ""

#: ../qml/MainPage.qml:316
msgid "Tap a die to lock its value during rerolls."
msgstr ""

#: ../qml/MainPage.qml:329
msgid "Total: %n"
msgstr "Totale: %n"

#: ../qml/MainPage.qml:338
#, fuzzy
#| msgid "Total: %n"
msgid "Total (%n+): %n"
msgstr "Totale: %n"

#: ../qml/MainPage.qml:405 ../qml/Components/DiceBottomEdge.qml:24
msgid "Collections & Custom Dice"
msgstr ""

#: ../qml/AboutPage.qml:54
msgid "A fork of the original Dice Roller app by Robert Ancell"
msgstr ""

#: ../qml/AboutPage.qml:62
msgid "Awesome icons by Joan CiberSheep (based on icons from the noun project)"
msgstr ""

#: ../qml/AboutPage.qml:70
msgid "Contributors: JassMan23 & AppsLee"
msgstr ""

#: ../qml/AboutPage.qml:79
msgid ""
"Forked and expanded by Brian Douglass, consider donating if you like it and "
"want to see more apps like it!"
msgstr ""

#: ../qml/AboutPage.qml:87
msgid "Donate"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:49
msgid "Settings"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:61
msgid "Threshold value:"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:77
msgid "Custom Dice"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:130
#: ../qml/Components/DiceBottomEdge.qml:208
msgid "Delete"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:140
msgid "Edit"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:145
#: ../qml/Components/DiceBottomEdge.qml:218
msgid "Add"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:177
#: ../qml/Dialogs/CustomDieDialog.qml:70
msgid "Create a custom die"
msgstr ""

#: ../qml/Components/DiceBottomEdge.qml:184
#, fuzzy
#| msgid "Dice Roller"
msgid "Dice Collections"
msgstr "Dice Roller"

#: ../qml/Components/DiceBottomEdge.qml:250
msgid "Create a collection"
msgstr ""

#: ../qml/Dialogs/CollectionNameDialog.qml:14
msgid "Choose a name for this collection of dice"
msgstr ""

#: ../qml/Dialogs/CollectionNameDialog.qml:29
#: ../qml/Dialogs/CustomDieDialog.qml:100
msgid "Save"
msgstr ""

#: ../qml/Dialogs/CollectionNameDialog.qml:40 ../qml/Dialogs/DiceDialog.qml:266
#: ../qml/Dialogs/CustomDieDialog.qml:107
msgid "Cancel"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:33
msgid "Pick a new die"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:47
msgid "coin"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:61
msgid "d4"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:75
msgid "d6"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:89
msgid "d8"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:103
msgid "d12"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:117
msgid "d20"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:131
msgid "d10 (1-10)"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:145
msgid "d10 (0-9)"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:159
msgid "d10 (00-90)"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:179
msgid "d100"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:193
msgid "Pick a custom die"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:225
msgid ""
"No custom dice available,\n"
"create some by swiping up from the bottom"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:233
msgid "Pick a collection"
msgstr ""

#: ../qml/Dialogs/DiceDialog.qml:257
msgid ""
"No collections available,\n"
"create one by swiping up from the bottom"
msgstr ""

#: ../qml/Dialogs/SimpleDialog.qml:17
msgid "Ok"
msgstr ""

#: ../qml/Dialogs/CustomDieDialog.qml:75
msgid "Name"
msgstr ""

#: ../qml/Dialogs/CustomDieDialog.qml:85
msgid ""
"Enter side values separated by commas. You can enter emojis directly from "
"the keyboard or use unicode for extra symbols and foreign characters,\n"
"e.g. U+278A, U+278B, U+278C ,U+278D\n"
"\n"
"Dots are special and produce a standard die. Precede first value with an SVG "
"color name if required,\n"
"e.g. [coral].,.,.,.,.,.)"
msgstr ""

#: dice-roller.desktop.in.h:2
msgid "Simulate rolling dice"
msgstr "Simula in giro dei dadi"

#: dice-roller.desktop.in.h:3
msgid "d4,d6,d8,d12,d20,d10,d100,die,dice,roll,random,chance,coin"
msgstr ""

#~ msgid "d6,die,dice,roll,random"
#~ msgstr "d6,die,dice,roll,random"
